#! /bin/sh

# Inspired by https://github.com/brianfarlinger/proxmox-templating/blob/master/first-config.sh

if ! [ $(id -u) = 0 ]; then
  echo "Execute as root"
  exit 1
fi

do_step() {
    dpkg-reconfigure openssh-server
}

do_step