#! /bin/sh

# Inspired by https://github.com/brianfarlinger/proxmox-templating/blob/master/clean-template.sh

if ! [ $(id -u) = 0 ]; then
  echo "Execute as root"
  exit 1
fi

echo -n "Are you sure? (y/N)"
read RES
if [ "x$RES" = "xy" ]; then
    apt-get clean
    logrotate -f /etc/logrotate.conf
    rm -rf /var/log/*-????????
    rm -rf /var/log/*.gz
    rm -rf /var/log/dmesg.old
    cat /dev/null > /var/log/wtmp
    cat /dev/null > /var/log/lastlog

    # rm -f /etc/netplan/*.yaml

    #remove temp files
    rm -rf /tmp/*
    rm -rf /var/tmp/*

    #remove SSH keys
    rm -f /etc/ssh/*key*

    rm -f /home/*/.bash_history

    # Machine-id fix: https://jaylacroix.com/fixing-ubuntu-18-04-virtual-machines-that-fight-over-the-same-ip-address/
    truncate -s 0 /etc/machine-id
    rm /var/lib/dbus/machine-id
    ln -s /var/lib/dbus/machine-id /etc/machine-id

    echo "Now reboot!"
fi