#! /bin/sh

# Inspired by https://github.com/brianfarlinger/proxmox-templating/blob/master/first-config.sh

if ! [ $(id -u) = 0 ]; then
  echo "Execute as root"
  exit 1
fi

#changing the hostname and configuring the network using NetworkManager.
do_step() {
    CUR_HOSTNAME=`cat /etc/hostname`
    echo -n "What is the new hostname of this server?: "
    read HOSTNAME
    hostnamectl set-hostname $HOSTNAME
    systemctl restart systemd-hostnamed
    sed -i "s/127.0.1.1 $CUR_HOSTNAME/127.0.1.1 $HOSTNAME/g" /etc/hosts
}

do_step